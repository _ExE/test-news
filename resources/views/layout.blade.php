<!doctype html>
<html lang="ru">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
          integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <title>Новости</title>
</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="content">
        @yield('content')
        <h3>Горячие новости</h3>
        @foreach ($newsCompilation as $oneNews)
            <li>
                <a href="{{ url('/news/'.$category->slug.'/'.$oneNews->id.'-'.$oneNews->slug.'.html' ) }}">
                    <img height="40" src="/storage/images/{{$oneNews->picture}}">
                    <b>{{ $oneNews->name }}</b><br>
                    ({{ $oneNews->created_at->format('d.m.Y H:i:s') }})
                    {{ $oneNews->annotation }}
                </a>
            </li>
        @endforeach
    </div>
</div>
</body>
</html>
