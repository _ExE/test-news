@extends('layout')

@section('content')
    <h1> Новости по категориям </h1>
    <div>
        <ul>
            @foreach ($categories as $category)
                <li>
                    <a href="{{ url('/news/'.$category->slug) }}">{{ $category->name }}
                        ({{ $category->publishedNews->count() }})</a>
                </li>
            @endforeach
        </ul>
    </div>
@endsection
