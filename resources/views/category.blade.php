@extends('layout')

@section('content')
    <h1>  {{ $category->name }} ({{ $category->publishedNews->count() }}) </h1>
    <div>
        <ul>
            @foreach ($news as $oneNews)
                <li>
                    <a href="{{ url('/news/'.$category->slug.'/'.$oneNews->id.'-'.$oneNews->slug.'.html' ) }}">
                        <img height="40" src="/storage/images/{{$oneNews->picture}}">
                        <b>{{ $oneNews->name }}</b><br>
                        ({{ $oneNews->created_at->format('d.m.Y H:i:s') }})
                        {{ $oneNews->annotation }}
                    </a>
                </li>
            @endforeach
        </ul>
    </div>
    {{ $news->links() }}
@endsection
