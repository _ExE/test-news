@extends('layout')

@section('content')
    <h1>  {{ $category->name }} </h1>
    <h2>  {{ $oneNews->name }}</h2>
    <h3> просмотров - {{ $oneNews->counter }} | опубликована - {{ $oneNews->created_at->format('d.m.Y H:i:s') }}</h3>
    <div>
        {!! $oneNews->content !!}
    </div>
@endsection
