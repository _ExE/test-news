<?php

namespace App\Http\Controllers;

use App\News;
use App\NewsCategory;
use App\Services\NewsService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Response;
use Illuminate\View\View;

class NewsController extends Controller
{
    private NewsService $newsService;

    /**
     * NewsController constructor.
     * @param NewsService $newsService
     */
    public function __construct(NewsService $newsService)
    {
        $this->newsService = $newsService;
    }

    /**
     * @return Application|Factory|Response|View
     */
    public function index()
    {
        $categories = NewsCategory::whereState(true)->with('publishedNews')->get();
        $newsCompilation = $this->newsService->getNewsCompilation();

        return view('home', compact('categories', 'newsCompilation'));
    }

    /**
     * @param string $slug
     * @return Application|Factory|View
     */
    public function category(string $slug)
    {
        $category = NewsCategory::whereSlug($slug)->firstOrFail();
        $news = $category->publishedNews()->simplePaginate(10);
        $newsCompilation = $this->newsService->getNewsCompilation($category);

        return view('category', compact('category', 'news', 'newsCompilation'));
    }

    /**
     * @param string $slug
     * @param int $newsId
     * @return Application|Factory|View
     */
    public function news(string $slug, int $newsId)
    {
        $category = NewsCategory::whereSlug($slug)->firstOrFail();
        /** @var News $oneNews */
        $oneNews = $category->publishedNews()->findOrFail($newsId);
        $newsCompilation = $this->newsService->getNewsCompilation($category, $oneNews);

        return view('news', compact('category', 'oneNews', 'newsCompilation'));
    }

}
