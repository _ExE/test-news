<?php

namespace App\Services;


use App\News;
use App\NewsCategory;
use Illuminate\Database\Eloquent\Collection;

class NewsService
{
    private const LIMIT_NEWS = 3;
    private const LIMIT_DAYS = 7;

    /**
     * @param NewsCategory|null $newsCategory
     * @param News|null $oneNews
     * @return Collection
     */
    public function getNewsCompilation(?NewsCategory $newsCategory = null, ?News $oneNews = null): Collection
    {
        $builder = News::whereState(true)
            ->where('created_at', '<', now()->subDays(static::LIMIT_DAYS))
            ->latest();
        if ($newsCategory !== null) {
            $builder = $builder->where('category_id', '=', $newsCategory->id);
        }
        if ($oneNews !== null) {
            $builder = $builder->where('id', '!=', $oneNews->id);
        }

        return $builder->limit(static::LIMIT_NEWS)->get();
    }

}
