<?php


Route::get('/', static function () {
    return redirect('/news');
})->name('home');

Route::get('/news', 'NewsController@index')->name('news.index');

Route::get('news/{categorySlug}', 'NewsController@category')
    ->where('categorySlug', '[0-9a-z\-]*')->name('news.category');

Route::get('news/{categorySlug}/{id}-{newsSlug}.html', 'NewsController@news')
    ->where('categorySlug', '[0-9a-z\-]*')->name('news.one');
