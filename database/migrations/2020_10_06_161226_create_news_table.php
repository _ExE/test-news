<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('news', static function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('category_id')->unsigned()->index();
            $table->string('name');
            $table->string('slug')->unique();
            $table->boolean('state')->index();
            $table->text('annotation');
            $table->text('content');
            $table->string('picture');
            $table->bigInteger('counter')->unsigned();
            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('news_categories')
                ->onDelete('cascade');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('news');
    }
}
