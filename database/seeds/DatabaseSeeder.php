<?php

use App\News;
use App\NewsCategory;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(NewsCategory::class, 10)->create()->each(static function (NewsCategory $category) {
            $category->news()->saveMany(factory(News::class, 30)->make());
        });
    }

}
