<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\News;
use Faker\Generator as Faker;
use Illuminate\Support\Str;


$factory->define(News::class, static function (Faker $faker) {
    $name = $faker->sentence(5);
    return [
        'name' => $name,
        'slug' => Str::slug($name),
        'state' => $faker->randomElement([0, 1, 1, 1, 1]),
        'annotation' => $faker->paragraph,
        'content' => $faker->randomHtml(),
        'picture' => $faker->image('public/storage/images', 300, 300, null, false),
        'counter' => $faker->randomNumber(),
        'created_at' => now()->subDays($faker->randomElement([6, 7, 8, 9, 10]),),
    ];
});
