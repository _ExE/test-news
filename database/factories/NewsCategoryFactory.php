<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\NewsCategory;
use Faker\Generator as Faker;
use Illuminate\Support\Str;


$factory->define(NewsCategory::class, static function (Faker $faker) {
    $name = $faker->sentence(5);
    return [
        'name' => $name,
        'slug' => Str::slug($name),
        'state' => $faker->randomElement([0, 1, 1, 1, 1]),
    ];
});
