<?php

namespace Tests\Feature;

use Tests\TestCase;

class RoutsTest extends TestCase
{
    public function testHome(): void
    {
        $name = 'home';
        $url = '/';
        $status = 302;

        $response1 = $this->get(route($name));
        $response1->assertStatus($status);
        $response2 = $this->get($url);
        $response2->assertStatus($status);
        $this->assertEquals($response1->content(), $response2->content());
    }

    public function testNewsIndex(): void
    {
        $name = 'news.index';
        $url = '/news';
        $status = 200;

        $response1 = $this->get(route($name));
        $response1->assertStatus($status);
        $response2 = $this->get($url);
        $response2->assertStatus($status);
        $this->assertEquals($response1->content(), $response2->content());
    }
}
